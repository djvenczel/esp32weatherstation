#ifndef _TCPSERVER_H_
#define _TCPSERVER_H_

#include <Arduino.h>
#include <WiFi.h>

class TCPServer
{
public:
    explicit TCPServer(int port = 11000);

    void startServer();
    void sendData(const String &data);

    bool isAlive() const;

private:
    WiFiServer mServer;
    WiFiClient mClient;
    bool isStarted;
};


#endif