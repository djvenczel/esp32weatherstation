#ifndef _BLEDEVICEINFO_H_
#define _BLEDEVICEINFO_H_

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>


#define SERVICE_DEVICE_INFO_UUID          BLEUUID((uint16_t)0x180A) 
#define CHARACTERISTIC_MANUFACTURER_STR   BLEUUID ((uint16_t)0x2A29)
#define CHARACTERISTIC_MODEL_STR          BLEUUID ((uint16_t)0x2A24)
#define CHARACTERISTIC_SERIAL_STR         BLEUUID ((uint16_t)0x2A25)
#define CHARACTERISTIC_HARDWARE_REV_STR   BLEUUID ((uint16_t)0x2A27)
#define CHARACTERISTIC_FIRMWARE_REV_STR   BLEUUID ((uint16_t)0x2A26)
#define CHARACTERISTIC_SOFTWARE_REV_STR   BLEUUID ((uint16_t)0x2A28)

#define CHARACTERISTIC_SYSTEM_ID          BLEUUID ((uint16_t)0x2A23)
#define CHARACTERISTIC_REGULATORY_LIST    BLEUUID ((uint16_t)0x2A2A)
#define CHARACTERISTIC_PNP_ID             BLEUUID ((uint16_t)0x2A50)

class BLEDeviceInfo {

public:
    BLEDeviceInfo(BLEServer *pServer, std::string manufacturer, uint8_t sig, uint16_t vid, uint16_t pid, uint16_t version);
    virtual ~BLEDeviceInfo();

    void start();

private:

   BLEService *mpdeviceInfoService;

    BLECharacteristic mpPnpCharacteristic;
   BLECharacteristic mpManufacturerCharacteristic;	
   

};


#endif /* _BLEDEVICEINFO_H_ */