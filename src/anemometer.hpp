#ifndef _ANEMOMETER_H_
#define _ANEMOMETER_H_

#include "task.hpp"
#include <Arduino.h>
#include "HardwareSerial.h"
#include "TCPServer.hpp"


class Anemometer : public Task {

private:
    const uint8_t STATION_ID = 0x02;
    const uint8_t READ_WIND_COMMAND = 0x03;

    const uint8_t READ_COMMAND[8] = { STATION_ID, READ_WIND_COMMAND, 0x00, 0x00, 0x00, 0x01, 0x84, 0x39 };

    typedef enum 
    {
        SENDING = 0,
        RECEIVING
    } RS485_STATE;

    typedef enum 
    {
        WAITING_FOR_STATION_NUMBER = 0,
        WAITING_FOR_COMMAND,
        WAITING_FOR_DATA_LENGTH,
        WAITING_FOR_DATA,
        WAITING_FOR_CRC_LOW,
        WAITING_FOR_CRC_HIGH
    } RS485_RECEIVING_STATE;

public:
    Anemometer(TCPServer *server = nullptr);
    virtual ~Anemometer();

    void requestData();
    
    const float getLastMeasurement() const;

    static int16_t calculateCRC(byte *data, size_t size);


private:
    HardwareSerial mSerial;
    RS485_STATE mSerialState;
    RS485_RECEIVING_STATE mReceivingState;

    float mLastMeasurement;
    TCPServer *mpTCPServer;

    

protected:
    void run(void *data);
};


#endif /* _ANEMOMETER_H_ */