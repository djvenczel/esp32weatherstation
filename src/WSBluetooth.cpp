#include "WSBluetooth.hpp"
#include <string.h>
#include "anemometer.hpp"


WSBluetooth::WSBluetooth(WSBluetoothCallbacks *callbacks)
    : mServerCallbacks(callbacks)
{
    this->start();
}

WSBluetooth::~WSBluetooth()
{

}

void WSBluetooth::startBluetooth(const char *deviceName)
{
    mSerialBT.begin(deviceName);     
}
    

void WSBluetooth::run( void *pvParameters )
{
    int receivedByte;
    uint16_t receivedDataLength = 0;
    uint8_t packetSize = 0;
    int16_t crc = 0;
    SERIAL_MESSAGE_TYPE serialMessage;

    mSerialState = SERIAL_STATE_WAIT_FOR_LEAD_IN;

    for (;;) {
        while (mSerialBT.available()) {
            receivedByte = mSerialBT.read();
            //Serial.printf("Received from BT in state %d, at %d value: %d \n", mSerialState, receivedDataLength, receivedByte);
            switch(mSerialState) {
                case SERIAL_STATE_WAIT_FOR_LEAD_IN:
                    if(receivedByte == SERIAL_LEAD_IN) {
                        serialMessage.data[receivedDataLength] = receivedByte;
                        receivedDataLength++;
                        mSerialState = SERIAL_STATE_WAIT_FOR_SIZE; 
                        //Serial.printf("Bluetooth lead in received, waiting for data size\n");
                    }                    
                    break;
                case SERIAL_STATE_WAIT_FOR_SIZE:
                    packetSize = (uint8_t)receivedByte;
                    if (packetSize > SERIAL_MAX_PACKET_SIZE) {
                        mSerialState = SERIAL_STATE_WAIT_FOR_LEAD_IN; 
                        receivedDataLength = 0;
                        //Serial.printf("Bluetooth packet size is bigger then allowed max packet size\n");
                    } else {
                        //Serial.printf("Bluetooth data size received, waiting for %d bytes \n", packetSize);
                        if(packetSize < 7) {
                            mSerialState = SERIAL_STATE_WAIT_FOR_LEAD_IN; 
                            receivedDataLength = 0;
                            //Serial.printf("Bluetooth packet size is smaller then required minimum size\n");
                        }
                        serialMessage.data[receivedDataLength] = packetSize;
                        receivedDataLength++;
                        mSerialState = SERIAL_STATE_WAIT_FOR_DATA; 
                    }
                    break;
                case SERIAL_STATE_WAIT_FOR_DATA:
                    serialMessage.data[receivedDataLength] = (uint8_t)receivedByte;
                    receivedDataLength++;
                    if(receivedDataLength == packetSize-3) {
                        mSerialState = SERIAL_STATE_WAIT_FOR_CRC; 
                        //Serial.printf("Bluetooth data received, waiting for crc\n");
                    }
                    break;
                case SERIAL_STATE_WAIT_FOR_CRC:
                    serialMessage.data[receivedDataLength] = (uint8_t)receivedByte;
                    receivedDataLength++;
                    if(receivedDataLength == (packetSize - 1)) {
                        mSerialState = SERIAL_STATE_WAIT_FOR_LEAD_OUT; 
                        //Serial.printf("Bluetooth crc received, waiting lead out\n");
                    }
                    break;
                case SERIAL_STATE_WAIT_FOR_LEAD_OUT:
                    if(receivedByte == SERIAL_LEAD_OUT) {
                        serialMessage.data[receivedDataLength] = SERIAL_LEAD_OUT;
                        int16_t dataCrc = ((int16_t)serialMessage.data[serialMessage.dataSize - 2] << 8) | serialMessage.data[serialMessage.dataSize - 3];
                        crc = Anemometer::calculateCRC((byte*)&serialMessage.data, serialMessage.dataSize - 3);
                        if(crc == dataCrc) {
                            ProcessBluetoothMessage(&serialMessage);
                        } else {
                            
                        }
                        
                    }
                    receivedDataLength = 0;
                    mSerialState = SERIAL_STATES::SERIAL_STATE_WAIT_FOR_LEAD_IN;
                    break;
            }
        }
        //UBaseType_t maxStack = uxTaskGetStackHighWaterMark(blueToothTaskHandle);
        //Serial.printf("Bluetooth task max stack size: %d \n", maxStack);
        vTaskDelay(10);
    }
}

void WSBluetooth::ProcessBluetoothMessage(SERIAL_MESSAGE_TYPE *serialMessage)
{
    SERIAL_MESSAGE_SETTINGS settingsMessage;
    switch (serialMessage->message)
    {
        case SERIAL_MESSAGE_GET_VERSION:
            Serial.printf("Bluetooth GET VERSION request received\n");
            mServerCallbacks->bluetoothMessageCallback(WSBluetoothCallbacks::GET_VERSION);
            break;
        case SERIAL_MESSAGE_GET_MEASUREMENT:
            Serial.printf("Bluetooth GET MEASUREMENT request received\n");
            mServerCallbacks->bluetoothMessageCallback(WSBluetoothCallbacks::GET_MEASUREMENT);
            break;
        case SERIAL_MESSAGE_GET_SETTINGS:
            Serial.printf("Bluetooth GET settings request received\n");
            mServerCallbacks->bluetoothMessageCallback(WSBluetoothCallbacks::GET_SETTINGS);
            break;
        case SERIAL_MESSAGE_SET_SETTINGS:
            memcpy(&settingsMessage, serialMessage, sizeof(SERIAL_MESSAGE_SETTINGS));
            Serial.printf("Bluetooth SET settings request received\n");
            Serial.printf(" Acces point name: %s \n", settingsMessage.accesPointName);
            Serial.printf(" Wifi password: %s \n", settingsMessage.password);
            Serial.printf(" Server name: %s \n", settingsMessage.serverName);
            Serial.printf(" Refresh interval: %u \n", settingsMessage.refreshInterval);
            Serial.printf(" Station ID: %u \n", settingsMessage.stationID);

            mServerCallbacks->bluetoothSettingsCallback(settingsMessage.accesPointName, settingsMessage.password, settingsMessage.serverName, settingsMessage.refreshInterval, settingsMessage.stationID);          
            break;
        case SERIAL_MESSAGE_RESTART_DEVICE:
            Serial.printf("Bluetooth restart device request received\n");
            mServerCallbacks->bluetoothMessageCallback(WSBluetoothCallbacks::RESTART_DEVICE);
            break;
        default:
            break;
    }
}

void WSBluetooth::sendSettings(const char *accesPointName, const char *password, const char *serverName, uint16_t refreshInterval, uint8_t stationID)
{
    SERIAL_MESSAGE_SETTINGS settingsMessage;

    settingsMessage.message = SERIAL_MESSAGE_GET_SETTINGS;

    settingsMessage.leadIn = SERIAL_LEAD_IN;
    settingsMessage.leadOut = SERIAL_LEAD_OUT;
    settingsMessage.dataSize = sizeof(SERIAL_MESSAGE_SETTINGS);   

    strcpy(settingsMessage.accesPointName, accesPointName);
    strcpy(settingsMessage.password, password);
    strcpy(settingsMessage.serverName, serverName);
    settingsMessage.refreshInterval =  refreshInterval;
    settingsMessage.stationID = stationID;

    settingsMessage.crc = Anemometer::calculateCRC((byte*)&settingsMessage.data, sizeof(SERIAL_MESSAGE_SETTINGS) - 3);

    mSerialBT.write(settingsMessage.data ,sizeof(SERIAL_MESSAGE_SETTINGS));
}

void WSBluetooth::sendMeasurement(const float temperature, const float humidity, const float pressure, const float wind, const float batteryVoltage, const uint8_t batteryLevel)
{
    SERIAL_MESSAGE_MEASUREMENT measurementMessage;

    measurementMessage.message = SERIAL_MESSAGE_GET_MEASUREMENT;
    measurementMessage.leadIn = SERIAL_LEAD_IN;
    measurementMessage.leadOut = SERIAL_LEAD_OUT;
    measurementMessage.dataSize = sizeof(SERIAL_MESSAGE_MEASUREMENT);   

    measurementMessage.temperature = temperature;
    measurementMessage.humidity = humidity;
    measurementMessage.pressure = pressure;
    measurementMessage.wind = wind;
    measurementMessage.batteryVoltage = batteryVoltage;
    measurementMessage.batteryLevel = batteryLevel;

    measurementMessage.crc = Anemometer::calculateCRC((byte*)&measurementMessage.data, sizeof(SERIAL_MESSAGE_MEASUREMENT) - 3);

    mSerialBT.write(measurementMessage.data ,sizeof(SERIAL_MESSAGE_MEASUREMENT));
}

void WSBluetooth::sendVersion(uint8_t version, uint8_t subversion)
{
    SERIAL_MESSAGE_VERSION versionMessage;

    versionMessage.message = SERIAL_MESSAGE_GET_VERSION;
    versionMessage.leadIn = SERIAL_LEAD_IN;
    versionMessage.leadOut = SERIAL_LEAD_OUT;
    versionMessage.dataSize = sizeof(SERIAL_MESSAGE_VERSION);   

    versionMessage.version = 1;
    versionMessage.subVersion = 0;

    versionMessage.crc = Anemometer::calculateCRC((byte*)&versionMessage.data, sizeof(SERIAL_MESSAGE_VERSION) - 3);

    mSerialBT.write(versionMessage.data ,sizeof(SERIAL_MESSAGE_VERSION));
}