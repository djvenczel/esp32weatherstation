/**
 * BasicHTTPClient.ino
 *
 *  Created on: 24.05.2015
 *
 */

#include <Arduino.h>
#include "main.h"
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <base64.h>
#include "EEPROM.h"

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include "esp32/ulp.h"
#include "driver/rtc_io.h"

#include <esp_adc_cal.h>

#include "anemometer.hpp"
#include "BLEBattery.hpp"
#include "BLEDeviceInfo.hpp"
#include "BLEWeatherStation.hpp"
#include "WSBluetooth.hpp"

#include "TCPServer.hpp"

#include "SolarCharger.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

/********************** Defines **********************************/
#define USE_SERIAL Serial
#define BLE
//#define CLASSIC_BT

#define DEFAULT_VREF 1100
#define BATTERY_SENSE_PIN 35
#define BATTERY_MULTIPLIER 4.3f

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

/********************** Variables **********************************/

WiFiMulti wifiMulti;
TCPServer tcpServer;
Adafruit_BME280 bme;

EEPROMClass  SettingsStorage("eeprom", 0x1000);   //Must be the same as in default.csv config file!
BLE_EEPROM_DATA eepromData;
Anemometer anemoMeter(&tcpServer);

bool tcpNotStarted = true;

BLEWeatherStation::BLE_WEATHER_DATA bleWeatherData;
#ifdef BLE
BLEServer *pServer = NULL;
BLEBattery *pBLEBattery = NULL;
BLEDeviceInfo *pBLEDeviceInfo = NULL;
BLEWeatherStation *pBLEWeather = NULL;
#endif

bool deviceConnected = false;
bool oldDeviceConnected = false;

TaskHandle_t blueToothTaskHandle;
TaskHandle_t sensorTaskHandle;

uint32_t batteryADCValue;
uint32_t ADCvoltage;
float batteryVoltage;
uint16_t batteryLevel = 0;
uint16_t batteryLevelOld = 0;
esp_adc_cal_characteristics_t adc_chars;

RTC_DATA_ATTR float maxWind = 0;       
esp_sleep_wakeup_cause_t wakeup_reason;

//SolarCharger solarCharger;

/********************** Function Declarations **********************************/
void TaskSensor (void *pvParameters );
void bluetoothMessageHandler(const WSBluetoothCallbacks::EVENT_MESSAGES *message);
void I2CScanner();

#ifdef BLE
class WeatherBleServerCallbacks : public BLEServerCallbacks, public BLEWeatherStationDelegate
{
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.print("Device connected!\n");
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.print("Device disconnected!\n");
    }

    void getSettings(BLE_EEPROM_DATA *settings) {
        memcpy(settings, &eepromData, sizeof(BLE_EEPROM_DATA));
    }

    void setSettings(BLE_EEPROM_DATA *settings) {
        Serial.print("Settings set requested!\n");
        memcpy(&eepromData, settings, sizeof(BLE_EEPROM_DATA));
        SettingsStorage.put(0, eepromData);
        SettingsStorage.commit();
    }

    void resetDevice() {
        ESP.restart();
    }
};
#endif
#ifdef CLASSIC_BT
class WeatherBluetoothCallbacks: public WSBluetoothCallbacks 
{
    void bluetoothMessageCallback(const EVENT_MESSAGES &message) {
        bluetoothMessageHandler(&message);
    }

    void bluetoothSettingsCallback(const char *accesPointName, const char *password, const char *serverName, uint16_t refreshInterval, uint8_t stationID) {

        strcpy(eepromData.accesPointName, accesPointName);
        strcpy(eepromData.wifiPassword, password);
        strcpy(eepromData.serverName, serverName);
        eepromData.refreshInterval = refreshInterval;
        eepromData.stationID = stationID;

        SettingsStorage.put(0, eepromData);
        SettingsStorage.commit();
    }
};
#endif

#ifdef BLE
WeatherBleServerCallbacks bleCallbacks;
#endif
#ifdef CLASSIC_BT
WeatherBluetoothCallbacks bluetoothCallbacks;
WSBluetooth blueTooth(&bluetoothCallbacks);
#endif

void setup() {
    bool startWifi = true;

    USE_SERIAL.begin(115200);
    USE_SERIAL.println();

    //adc2_vref_to_gpio(GPIO_NUM_25);
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, DEFAULT_VREF, &adc_chars);
    analogSetPinAttenuation(BATTERY_SENSE_PIN, ADC_11db);
    analogSetClockDiv(10);

    //Init local storage
    if (!SettingsStorage.begin(SettingsStorage.length())) {
        USE_SERIAL.println("Failed to initialise SettingsStorage");
        USE_SERIAL.println("Restarting...");
        delay(1000);
        ESP.restart();
    }

    SettingsStorage.get(0, eepromData);

    if(eepromData.accesPointName[0] == 0xFF ||
        eepromData.wifiPassword[0] == 0xFF ||
        eepromData.serverName[0] == 0xFF ||
        strlen(eepromData.accesPointName) == 0 || 
        strlen(eepromData.wifiPassword) == 0 || 
        strlen(eepromData.serverName) == 0) {
            USE_SERIAL.println("No valid configuration, starting only bluetooth service");
            startWifi = false;
    }

#ifdef BLE
    // Create the BLE Device
    BLEDevice::init("BLE Weather Station");

    // Create the BLE Server
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(&bleCallbacks);

    //Add services
    pBLEBattery = new BLEBattery(pServer);
    pBLEBattery->start();
    pBLEBattery->setBatteryLevel(100);

    pBLEDeviceInfo = new BLEDeviceInfo(pServer, "Venczel's Weather Station", 2, 0XFFFF, 0xFFFF, 0X0001);
    pBLEDeviceInfo->start();

    pBLEWeather = new BLEWeatherStation(pServer, &bleCallbacks, &USE_SERIAL);
    pBLEWeather->start();

    // Start advertising
    pServer->getAdvertising()->start();
#endif
#ifdef CLASSIC_BT
    blueTooth.startBluetooth("Weather Station");
#endif

    uint8_t status = bme.begin(); 
    if (!status) {
    USE_SERIAL.println("Could not find a valid BME280 sensor, check wiring! Program halting....");
        I2CScanner();
        while (1);
    }

    bme.setSampling(Adafruit_BME280::MODE_FORCED);

    if(startWifi) {
        //wifiMulti.addAP("0166f8", "274716892");
        wifiMulti.addAP(eepromData.accesPointName, eepromData.wifiPassword);
        xTaskCreatePinnedToCore(
            TaskSensor
            ,  "TaskSensor"   // A name just for humans
            ,  8192  // This stack size can be checked & adjusted by reading the Stack Highwater
            ,  NULL
            ,  3  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
            ,  &sensorTaskHandle 
            ,  ARDUINO_RUNNING_CORE);

        //tcpServer.startServer();
    }
    
}

#ifdef CLASSIC_BT
void bluetoothMessageHandler(const WSBluetoothCallbacks::EVENT_MESSAGES *message)
{
    switch (*message)
    {
        case WSBluetoothCallbacks::EVENT_MESSAGES::GET_VERSION:
            blueTooth.sendVersion(1,0);
            break;
        case WSBluetoothCallbacks::EVENT_MESSAGES::GET_MEASUREMENT:
            blueTooth.sendMeasurement(bleWeatherData.temperature, bleWeatherData.humidity, bleWeatherData.pressure, bleWeatherData.wind, bleWeatherData.batteryVoltage, bleWeatherData.batteryLevel);
            break;
        case WSBluetoothCallbacks::EVENT_MESSAGES::GET_SETTINGS:
            blueTooth.sendSettings(eepromData.accesPointName, eepromData.wifiPassword, eepromData.serverName, eepromData.refreshInterval, eepromData.stationID);
            break;
        case WSBluetoothCallbacks::EVENT_MESSAGES::RESTART_DEVICE:
            ESP.restart();
            break;
        default:
            break;
    }
}
#endif

void loop() {
    batteryADCValue = analogRead(BATTERY_SENSE_PIN);
    ADCvoltage = esp_adc_cal_raw_to_voltage(batteryADCValue, &adc_chars);
    batteryVoltage =  ADCvoltage * BATTERY_MULTIPLIER / 1000.0f;
    batteryLevel = map(batteryVoltage * 10, 36, 42, 0, 100);
    
    //USE_SERIAL.printf("ADC Raw: %d ADC Voltage: %d batteryVoltage: %f, batteryLevel: %d \n", batteryADCValue, ADCvoltage, batteryVoltage, batteryLevel);

    anemoMeter.requestData();

    if(batteryLevelOld != batteryLevel) {
        batteryLevelOld = batteryLevel;
#ifdef BLE        
        pBLEBattery->setBatteryLevel(batteryLevel);
#endif
    }
    
    bme.takeForcedMeasurement();
    bleWeatherData.humidity = bme.readHumidity();
    bleWeatherData.pressure = bme.readPressure() / 100.0f;
    bleWeatherData.temperature = bme.readTemperature();
    bleWeatherData.wind = anemoMeter.getLastMeasurement();

    bleWeatherData.batteryLevel = batteryLevel;
    bleWeatherData.batteryVoltage = batteryVoltage;

#ifdef BLE
    pBLEWeather->sendWeatherData(bleWeatherData);
#endif

    if(bleWeatherData.wind > maxWind) {
        maxWind = bleWeatherData.wind;
    }

    vTaskDelay(100);
}

void TaskSensor ( void *pvParameters )
{
    char SensorData[255] = "/0";
    String EncodedSensorData;
    String url = (String)eepromData.serverName + "?data=";

    vTaskDelay(100);

    for (;;) {
        SensorData[0] = 0;

        sprintf(SensorData,"{\"temperature\": %f ,\"pressure\": %f, \"humidity\" : %f, \"wind\" : %f, \"battery\" : %f, \"location\" : %d }", (double)bleWeatherData.temperature, (double)bleWeatherData.pressure, (double)bleWeatherData.humidity, (double)maxWind, (double)bleWeatherData.batteryVoltage, eepromData.stationID);
        EncodedSensorData = base64::encode((uint8_t*)SensorData, strlen(SensorData));

        USE_SERIAL.printf("Measurement: %f °C, %f %%, %f Hpa %f m/s \n", bleWeatherData.temperature, bleWeatherData.humidity, bleWeatherData.pressure, maxWind);

        maxWind = 0;

        // wait for WiFi connection
        if((wifiMulti.run() == WL_CONNECTED)) {

            HTTPClient http;

            if(tcpNotStarted) {
                USE_SERIAL.print("TCP Server starting: ");
                USE_SERIAL.println(WiFi.localIP());
                tcpServer.startServer();
                tcpNotStarted = false;
            }

            USE_SERIAL.print("[HTTP] begin...\n");
            // configure traged server and url


            http.begin(url + EncodedSensorData); //HTTP

            USE_SERIAL.print("[HTTP] GET...\n");
            // start connection and send HTTP header
            int httpCode = http.GET();

            // httpCode will be negative on error
            if(httpCode > 0) {
                // HTTP header has been send and Server response header has been handled
                USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

                // file found at server
                if(httpCode == HTTP_CODE_OK) {
                    String payload = http.getString();
                    USE_SERIAL.println(payload);
                }
            } else {
                USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
            }

            http.end();
        }
        //UBaseType_t maxStack = uxTaskGetStackHighWaterMark(sensorTaskHandle);
        //USE_SERIAL.printf("Sensor task max stack size: %d \n", maxStack);

        vTaskDelay(eepromData.refreshInterval);
        //esp_sleep_enable_timer_wakeup(500000);  //500ms
        //esp_deep_sleep_start();
    }
}

void I2CScanner()
{
  byte error, address;
  int nDevices;
 
  USE_SERIAL.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
        USE_SERIAL.print("I2C device found at address 0x");
        if (address<16)
        USE_SERIAL.print("0");
        USE_SERIAL.print(address,HEX);
        USE_SERIAL.println("  !");

        Wire.beginTransmission(address);
        Wire.write((uint8_t)0xD0);
        Wire.endTransmission();
        Wire.requestFrom((uint8_t)address, (byte)1);
        uint8_t value = Wire.read();
        USE_SERIAL.printf("Chip ID: %d", (uint16_t)value);

        nDevices++;
    }
    else if (error==4)
    {
      USE_SERIAL.print("Unknown error at address 0x");
      if (address<16)
        USE_SERIAL.print("0");
      USE_SERIAL.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    USE_SERIAL.println("No I2C devices found\n");
  else
    USE_SERIAL.println("done\n");
}