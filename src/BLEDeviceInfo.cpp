#include "BLEDeviceInfo.hpp"

BLEDeviceInfo::BLEDeviceInfo(BLEServer *pServer, std::string manufacturer, uint8_t sig, uint16_t vid, uint16_t pid, uint16_t version)
    : mpdeviceInfoService(0)
    , mpPnpCharacteristic(CHARACTERISTIC_PNP_ID, BLECharacteristic::PROPERTY_READ)
    , mpManufacturerCharacteristic(CHARACTERISTIC_MANUFACTURER_STR, BLECharacteristic::PROPERTY_READ)
{
    mpdeviceInfoService = pServer->createService(SERVICE_DEVICE_INFO_UUID);
    mpdeviceInfoService->addCharacteristic(&mpPnpCharacteristic);
    mpdeviceInfoService->addCharacteristic(&mpManufacturerCharacteristic);

    uint8_t pnp[] = { sig, (uint8_t) (vid >> 8), (uint8_t) vid, (uint8_t) (pid >> 8), (uint8_t) pid, (uint8_t) (version >> 8), (uint8_t) version };
	mpPnpCharacteristic.setValue(pnp, sizeof(pnp));

    mpManufacturerCharacteristic.setValue(manufacturer);

    pServer->getAdvertising()->addServiceUUID(SERVICE_DEVICE_INFO_UUID);
}

BLEDeviceInfo::~BLEDeviceInfo()
{

}

void BLEDeviceInfo::start()
{
    mpdeviceInfoService->start();
}