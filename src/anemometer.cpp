#include "anemometer.hpp"


Anemometer::Anemometer(TCPServer *server)
    : mSerial(2)
    , mSerialState(RECEIVING)
    , mReceivingState(WAITING_FOR_STATION_NUMBER)
    , mpTCPServer(server)
{
    mSerial.begin(9600, SERIAL_8N1);
    this->start();
}


Anemometer::~Anemometer()
{

}

void Anemometer::run(void *threadData)
{
    uint8_t CRCLow = 0;
    uint8_t CRCHigh = 0;
    uint8_t data[259];
    uint8_t dataLength = 0;
    uint8_t receivedDataLength = 0;
    uint16_t windSpeed;
    char debugStr[255] = "/0";

    for(;;) {
        sleep(1);
        while(mSerial.available()) {
            uint8_t receivedByte = (uint8_t)mSerial.read();
            if(mSerialState == RECEIVING)
                {
                    switch(mReceivingState)
                    {
                        case WAITING_FOR_STATION_NUMBER:
                            if(receivedByte == STATION_ID)
                            {
                                data[0] = receivedByte;
                                mReceivingState = WAITING_FOR_COMMAND;
                            }
                            else
                            {
                                mReceivingState = WAITING_FOR_STATION_NUMBER;
                            }
                            
                            break;
                        case WAITING_FOR_COMMAND:
                            if (receivedByte == READ_WIND_COMMAND) {
                                data[1] = receivedByte;
                                mReceivingState = WAITING_FOR_DATA_LENGTH;
                            } else {
                                mReceivingState = WAITING_FOR_STATION_NUMBER;
                            }
                            break;
                        case WAITING_FOR_DATA_LENGTH:
                            dataLength = receivedByte;
                            data[2] = receivedByte;
                            if (dataLength != 2 || dataLength == 0) {
                                mReceivingState = WAITING_FOR_STATION_NUMBER;
                                break;
                            }
                            receivedDataLength = 0;
                            mReceivingState = WAITING_FOR_DATA;
                            break;
                        case WAITING_FOR_DATA:
                            data[receivedDataLength + 3] = receivedByte;
                            receivedDataLength++;
                            if(receivedDataLength == dataLength) {
                                receivedDataLength++;
                                mReceivingState = WAITING_FOR_CRC_LOW;
                            }
                            break;
                        case WAITING_FOR_CRC_LOW:
                            CRCLow = receivedByte;
                            mReceivingState = WAITING_FOR_CRC_HIGH;
                            break;
                        case WAITING_FOR_CRC_HIGH:
                            CRCHigh = receivedByte;
                            mReceivingState = WAITING_FOR_STATION_NUMBER;
                            mSerialState = SENDING;
                            int16_t receivedCRC = (((int16_t)CRCHigh << 8) | CRCLow);
                            int16_t calcCRC = calculateCRC(data, receivedDataLength + 2);
                            if(receivedCRC == calcCRC)
                            {
                                windSpeed = (uint16_t)((data[3] << 8) | data[4]);
                                if(windSpeed == 0) {
                                    mLastMeasurement = 0.0f;
                                } else {
                                    mLastMeasurement = windSpeed / 10.0f;
                                }

                                //debug
                                if(mpTCPServer && mpTCPServer->isAlive()) { 
                                    sprintf(debugStr, "Anemometer measured: %f Data: %02X %02X %02X %02X %02X %02X %02X", windSpeed / 10.0f, data[0], data[1], data[2], data[3], data[4], CRCLow, CRCHigh);
                                    mpTCPServer->sendData(debugStr);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    receivedDataLength++;
                    if(receivedDataLength == sizeof(READ_COMMAND))
                    {
                        mSerialState = RECEIVING;
                    }
                }
        }
    }
}

void Anemometer::requestData()
{
    mSerialState = SENDING; //If RS485 converter available, this should be RECEIVING
    mReceivingState = WAITING_FOR_STATION_NUMBER;
    
    mSerial.write(READ_COMMAND, sizeof(READ_COMMAND));
}

const float Anemometer::getLastMeasurement() const
{
    return mLastMeasurement;
}

int16_t Anemometer::calculateCRC(uint8_t *data, size_t size)
{
    int i, n;
    uint8_t CRC16Lo, CRC16Hi;
    uint8_t SaveHi, SaveLo;
    CRC16Lo = 0xFF;
    CRC16Hi = 0XFF;
    for (i = 0; i < size; i++)
    {
        CRC16Lo = CRC16Lo ^ data[i]; 
        for (n = 0; n < 8; n++)
        {
            SaveHi = CRC16Hi;
            SaveLo = CRC16Lo;
            CRC16Hi >>= 1; 
            CRC16Lo >>= 1; 
            if ((SaveHi & 1) == 1)
            {
                CRC16Lo |= 0x80;
            }
            if ((SaveLo & 1) == 1) 
            {
                CRC16Hi ^= 0xA0;
                CRC16Lo ^= 1;
            }
        }
    }
    return ((int16_t)(CRC16Hi << 8) | CRC16Lo);
}