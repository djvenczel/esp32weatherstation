#include "TCPServer.hpp"


TCPServer::TCPServer(int port) 
    : mServer(port)
    , mClient ()
    , isStarted (false)
{
}

void TCPServer::startServer() {
    if(!isStarted) {
        mServer.begin();
        isStarted = true;
    }
}

void TCPServer::sendData(const String &data)
{
    if( mClient && mClient.connected()) {
         mClient.println(data);
    } else {
        mClient = mServer.available();
        if(mClient.connected()) {
            mClient.println(data);
        }
    }
}

bool TCPServer::isAlive() const
{
    return isStarted;
}