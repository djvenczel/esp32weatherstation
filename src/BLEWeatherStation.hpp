#ifndef _BLEWEATHERSTATION_H_
#define _BLEWEATHERSTATION_H_

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <HardwareSerial.h>

#define SERVICE_UUID                 "2CA703E0-375D-11E9-B56E-0800200C9A66"
#define DATA_CHARACTERISTIC_UUID     "2CA703E1-375D-11E9-B56E-0800200C9A66"
#define DATA_DESCRIPTOR_UUID         "2CA703E2-375D-11E9-B56E-0800200C9A66"
#define SETTINGS_CHARACTERISTIC_UUID "2CA703E3-375D-11E9-B56E-0800200C9A66"
#define SETTINGS_DESCRIPTOR_UUID     "2CA703E4-375D-11E9-B56E-0800200C9A66"
#define COMMAND_CHARACTERISTIC_UUID  "2CA703E5-375D-11E9-B56E-0800200C9A66"

#pragma pack(1)
typedef struct {
    char accesPointName[30];
    char wifiPassword[30];
    char serverName[60];
    uint16_t refreshInterval;
    uint8_t stationID; 
} __attribute((__packed__)) BLE_EEPROM_DATA;
#pragma pack(0)

class BLEWeatherStationDelegate
{
public:
    virtual ~BLEWeatherStationDelegate() {}

    virtual void getSettings(BLE_EEPROM_DATA *settings) = 0;
    virtual void setSettings(BLE_EEPROM_DATA *settings) = 0;
    virtual void resetDevice() = 0;
};

class BLEWeatherStation: public BLECharacteristicCallbacks {

public:

    typedef enum {
        BLE_WEATHER_COMMAND_NONE = 0,
        BLE_WEATHER_COMMAND_RESET
    } BLE_WEATHER_COMMAND;

    #pragma pack(1)
    typedef struct {
        float temperature;
        float pressure;
        float humidity;
        float wind;
        float batteryVoltage;
        uint16_t batteryLevel;
    } __attribute((__packed__)) BLE_WEATHER_DATA;
    #pragma pack(0)

public:
    BLEWeatherStation(BLEServer *pServer, BLEWeatherStationDelegate *delegate = nullptr, HardwareSerial *serial = nullptr);
    virtual ~BLEWeatherStation();

    void sendWeatherData(const BLE_WEATHER_DATA &level);
    void start();

    void onWrite(BLECharacteristic *pCharacteristic) override;
    void onRead(BLECharacteristic *pCharacteristic) override;

private:
    BLEService *mpService;
    BLECharacteristic mDataCharacteristic;
    BLEDescriptor mDataDescriptor;
    BLECharacteristic mSettingsCharacteristic;
    BLEDescriptor mSettingsDescriptor;
    BLECharacteristic mCommandCharacteristic;

    BLE_WEATHER_DATA mBLEWeatherData;
    BLE_EEPROM_DATA mBLESettings;

    BLEWeatherStationDelegate *mpDeleagate;
    HardwareSerial *mpSerial;

};


#endif /* _BLEWEATHERSTATION_H_ */