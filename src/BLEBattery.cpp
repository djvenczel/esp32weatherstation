#include "BLEBattery.hpp"

BLEBattery::BLEBattery(BLEServer *pServer)
    : mpBattery(0)
    , mBatteryLevelCharacteristic(CHARACTERISTIC_BATTERY_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY)
    , mBatteryLevelDescriptor(DESCRIPTOR_BATTERY_UUID)
{
    mpBattery = pServer->createService(SERVICE_BATTERY_UUID);
    mpBattery->addCharacteristic(&mBatteryLevelCharacteristic);

    mBatteryLevelDescriptor.setValue("Percentage 0 - 100");
    mBatteryLevelCharacteristic.addDescriptor(&mBatteryLevelDescriptor);
    mBatteryLevelCharacteristic.addDescriptor(new BLE2902());

    pServer->getAdvertising()->addServiceUUID(SERVICE_BATTERY_UUID);
}

BLEBattery::~BLEBattery()
{

}


void BLEBattery::setBatteryLevel(uint8_t level)
{
    mLevel = level;
    mBatteryLevelCharacteristic.setValue(&mLevel, 1);
    mBatteryLevelCharacteristic.notify();
}

void BLEBattery::start()
{
    mpBattery->start();
}