#include "BLEWeatherStation.hpp"

BLEWeatherStation::BLEWeatherStation(BLEServer *pServer, BLEWeatherStationDelegate *delegate, HardwareSerial *serial)
    : mpService(0)
    , mDataCharacteristic(DATA_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY)
    , mDataDescriptor(DATA_DESCRIPTOR_UUID)
    , mSettingsCharacteristic(SETTINGS_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE)
    , mSettingsDescriptor (SETTINGS_DESCRIPTOR_UUID)
    , mCommandCharacteristic(COMMAND_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_WRITE)
    , mpDeleagate (delegate)
    , mpSerial (serial)
{
    mpService = pServer->createService(SERVICE_UUID);
    mpService->addCharacteristic(&mDataCharacteristic);
    mpService->addCharacteristic(&mSettingsCharacteristic);
    mpService->addCharacteristic(&mCommandCharacteristic);

    mDataCharacteristic.addDescriptor(new BLE2902());
    mDataDescriptor.setValue("Weather data");
    mDataCharacteristic.addDescriptor(&mDataDescriptor);

    mSettingsDescriptor.setValue("Weather settings");
    mSettingsCharacteristic.addDescriptor(&mSettingsDescriptor);
    mSettingsCharacteristic.setCallbacks(this);

    mCommandCharacteristic.setCallbacks(this);

    if(mpDeleagate) {
        mpDeleagate->getSettings(&mBLESettings);
    }
    mSettingsCharacteristic.setValue((uint8_t*)&mBLESettings, sizeof(BLE_EEPROM_DATA));

    pServer->getAdvertising()->addServiceUUID(SERVICE_UUID);
}

BLEWeatherStation::~BLEWeatherStation()
{

}

void BLEWeatherStation::sendWeatherData(const BLE_WEATHER_DATA &data)
{
    mBLEWeatherData = data;
    mDataCharacteristic.setValue((uint8_t*)&mBLEWeatherData, sizeof(BLE_WEATHER_DATA));
    mDataCharacteristic.notify();
}

void BLEWeatherStation::onWrite(BLECharacteristic *pCharacteristic) 
{
    if(pCharacteristic->getUUID().equals(BLEUUID(SETTINGS_CHARACTERISTIC_UUID))) {
        std::string strData = pCharacteristic->getValue();
        uint8_t *data = pCharacteristic->getData();

        /*if(mpSerial) {
            mpSerial->printf("BLE Settings received packet length: %d, needed packet length: %d \r\n", strData.length(), sizeof(BLE_EEPROM_DATA));
            for(int i = 0; i < strData.length(); i++) {
                    mpSerial->printf("%d ", data[i]);
            }
            mpSerial->printf("\r\n");
        }*/

        if(strData.length() == sizeof(BLE_EEPROM_DATA)) {
            memcpy(&mBLESettings, data, sizeof(BLE_EEPROM_DATA));
            if(mpSerial) {
                mpSerial->printf("Settings Data: - access point: %s  - password: %s, -server: %s -refreshRate: %d, stationID: %d\n", mBLESettings.accesPointName, mBLESettings.wifiPassword, mBLESettings.serverName, mBLESettings.refreshInterval, mBLESettings.stationID);
            }

            if(mpDeleagate) {
                mpDeleagate->setSettings(&mBLESettings);
            }
        }
    } else if(pCharacteristic->getUUID().equals(BLEUUID(COMMAND_CHARACTERISTIC_UUID))) {
        uint8_t *data = pCharacteristic->getData();
        if(*data == BLE_WEATHER_COMMAND_RESET) {
            if(mpDeleagate) {
                mpDeleagate->resetDevice();
            }
        }
    }
}

void  BLEWeatherStation::onRead(BLECharacteristic *pCharacteristic)
{

}

void BLEWeatherStation::start()
{
    mpService->start();
}