
#ifndef _WSBLUETOOTH_H_
#define _WSBLUETOOTH_H_

#include <Arduino.h>
#include <stdint.h>
#include "BluetoothSerial.h"
#include "task.hpp"

    #define SERIAL_LEAD_IN 0xAA
    #define SERIAL_LEAD_OUT 0xCC
    #define SERIAL_MAX_PACKET_SIZE 129


class WSBluetoothCallbacks
{

public:
    typedef enum {
        GET_VERSION = 0,
        GET_MEASUREMENT,
        GET_SETTINGS,
        RESTART_DEVICE
    } EVENT_MESSAGES;

public:
    virtual void bluetoothMessageCallback(const EVENT_MESSAGES &message) = 0;
    virtual void bluetoothSettingsCallback(const char *accesPointName, const char *password, const char *serverName, uint16_t refreshInterval, uint8_t stationID) = 0;
};

class WSBluetooth : Task
{

private:

    typedef enum {
        SERIAL_STATE_WAIT_FOR_LEAD_IN = 0,
        SERIAL_STATE_WAIT_FOR_SIZE,
        SERIAL_STATE_WAIT_FOR_DATA,
        SERIAL_STATE_WAIT_FOR_CRC,
        SERIAL_STATE_WAIT_FOR_LEAD_OUT
    } SERIAL_STATES;

    typedef enum {
        SERIAL_MESSAGE_GET_VERSION = 0,
        SERIAL_MESSAGE_GET_MEASUREMENT,
        SERIAL_MESSAGE_GET_SETTINGS,
        SERIAL_MESSAGE_SET_SETTINGS,
        SERIAL_MESSAGE_RESTART_DEVICE
    } SERIAL_MESSAGES;

    #pragma pack(1)
    typedef union {
        struct {
            uint8_t leadIn;
            uint8_t dataSize;
            uint8_t message;
            uint8_t dataChunk[SERIAL_MAX_PACKET_SIZE-6];
            int16_t crc;
            uint8_t leadOut;
        };
        uint8_t data[SERIAL_MAX_PACKET_SIZE];
    } SERIAL_MESSAGE_TYPE;
    #pragma pack(0)


    #pragma pack(1)
    typedef union {
        struct {
            uint8_t leadIn;
            uint8_t dataSize;
            uint8_t message;
            uint8_t version;
            uint8_t subVersion;
            int16_t crc;
            uint8_t leadOut;
        };
        uint8_t data[8];
    } SERIAL_MESSAGE_VERSION;
    #pragma pack(0)

    #pragma pack(1)
    typedef union {
        struct {
            uint8_t leadIn;
            uint8_t dataSize;
            uint8_t message;
            float temperature;
            float humidity;
            float pressure;
            float wind;
            float batteryVoltage;
            uint8_t batteryLevel;
            int16_t crc;
            uint8_t leadOut;
        };
        uint8_t data[27];
    } SERIAL_MESSAGE_MEASUREMENT;
    #pragma pack(0)

    #pragma pack(1)
    typedef union {
        struct {
            uint8_t leadIn;
            uint8_t dataSize;
            uint8_t message;
            char accesPointName[30];
            char password[30];
            char serverName[60];
            uint16_t refreshInterval;
            uint8_t stationID;
            int16_t crc;
            uint8_t leadOut;
        };
        uint8_t data[129];
    } SERIAL_MESSAGE_SETTINGS;
    #pragma pack(0)

    #pragma pack(1)
    typedef union {
        struct {
            uint8_t leadIn;
            uint8_t dataSize;
            uint8_t message;
            int16_t crc;
            uint8_t leadOut;
        };
        uint8_t data[6];
    } SERIAL_MESSAGE_RESTART;
    #pragma pack(0)


public:
    WSBluetooth(WSBluetoothCallbacks *callbacks);
    virtual ~WSBluetooth();

    void startBluetooth(const char *deviceName);

    void sendSettings(const char *accesPointName, const char *password, const char *serverName, uint16_t refreshInterval, uint8_t stationID);
    void sendMeasurement(const float temperature, const float humidity, const float pressure, const float wind, const float batteryVoltage, const uint8_t batteryLevel);
    void sendVersion(uint8_t version, uint8_t subversion);

private:
    void ProcessBluetoothMessage(SERIAL_MESSAGE_TYPE *serialMessage);

protected:
    void run(void *threadData) override;

private:
    WSBluetoothCallbacks *mServerCallbacks;
    SERIAL_STATES mSerialState;
    BluetoothSerial mSerialBT;

};


#endif /* _WSBLUETOOTH_H_ */