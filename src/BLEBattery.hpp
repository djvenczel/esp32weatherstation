#ifndef _BLEBATTERY_H_
#define _BLEBATTERY_H_

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#define SERVICE_BATTERY_UUID          BLEUUID((uint16_t)0x180F) 
#define CHARACTERISTIC_BATTERY_UUID   BLEUUID ((uint16_t)0x2A19)
#define DESCRIPTOR_BATTERY_UUID       BLEUUID((uint16_t)0x2901)

class BLEBattery {

public:
    BLEBattery(BLEServer *pServer);
    virtual ~BLEBattery();

    void setBatteryLevel(uint8_t level);
    void start();

private:
    BLEService *mpBattery;
    BLECharacteristic mBatteryLevelCharacteristic;
    BLEDescriptor mBatteryLevelDescriptor;

    uint8_t mLevel;
};


#endif /* _BLEBATTERY_H_ */